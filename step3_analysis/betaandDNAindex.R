#Date: 21-Dec-2021
#Partially constrained ordination analysis


library(phyloseq)
library(ggplot2)
library(ggrepel)
library(tidyverse)
library(ranacapa)
library(vegan)

primer <- "16S"

physeq_mainDS <- readRDS(paste0("topanga/output/",primer,"/physeq_mainDS_",primer,".Rds"))


#Transform to vegan

sampleDF <- data.frame(sample_data(physeq_mainDS))
sampleDF$Site <- factor(sampleDF$Site)
sampleDF$Bio_rep <- factor(sampleDF$Bio_rep)

veganOTU <- vegan_otu(physeq_mainDS)

#Transforms number of reads to eDNA index

veganOTU_index <- wisconsin(veganOTU)


#constrained ordination

tmp <- capscale(veganOTU_index ~ sampleDF$Site, distance = "bray")
tmp


#Add spps info to results

sppscores(tmp) <- veganOTU_index

#make data frame of top species for plotting

as.data.frame(vegan::scores(tmp, display="species")) %>% 
  rownames_to_column(var = "sample") %>% 
  as.tibble %>% 
  mutate(dist = sqrt((CAP1 - 0)^2 + (CAP2 - 0)^2)) %>% 
  top_n(6, dist) -> top_species

top_species <- as.data.frame(top_species)
rownames(top_species) <- top_species$sample

#Now add the spps variables as arrows

arrowmat <- top_species

#Rename row names to clean up plot

as.data.frame(rownames(arrowmat)) %>% 
  separate("rownames(arrowmat)", into = c("superkingdom", "phylum","class", "order","famliy","genus", "species") , sep = "\\;", remove = F) %>% #split by name
  select(species) -> names

names <- transmute(names, species = as.character(gsub("(\\w)*:(\\w)", "\\2", species)))
rownames(arrowmat) <- names[,1]

#Add labels, make a data.frame

arrowdf <- data.frame(labels = rownames(arrowmat), arrowmat)

#Define the arrow aesthetic mapping

arrow_map <- aes(xend = CAP1, 
                 yend = CAP2, 
                 x = 0, 
                 y = 0, 
                 shape = NULL, 
                 color = NULL, 
                 label = labels)

label_map <- aes(x = 0.75 * CAP1, #.75* 
                 y = 1.3 * CAP2, #1.3*
                 shape = NULL, 
                 color = NULL, 
                 label = labels)

arrowhead = arrow(length = unit(0.02, "npc"))

#Hulls for each polygon

data.scores_1 <- as.data.frame(tmp$CCA$wa)
data.scores_1$Site <- sampleDF$Site  

head(data.scores_1) 


#### Calculate Shape Around Points

NF3 <- data.scores_1[data.scores_1$Site == "No-freezing_3", ][chull(data.scores_1[data.scores_1$Site == "No-freezing_3", c("CAP1", "CAP2")]), ]  
PF3 <- data.scores_1[data.scores_1$Site == "Pre-freezing_3", ][chull(data.scores_1[data.scores_1$Site == "Pre-freezing_3", c("CAP1", "CAP2")]), ]
Sed <- data.scores_1[data.scores_1$Site == "Sediment", ][chull(data.scores_1[data.scores_1$Site == "Sediment", c("CAP1", "CAP2")]), ]

hull.data <- rbind(NF3, PF3, Sed)

#plot

#getting percent variation for each axis

kip <- 100 * tmp$CCA$eig/sum(tmp$CCA$eig)


cap_plot <- ggplot(hull.data, aes(x=CAP1, y=CAP2)) +
  geom_point(aes(col=Site), size =3) +
  geom_segment(mapping = arrow_map, size = .5, data = arrowdf, color = "grey", arrow = arrowhead) +
  #geom_text(mapping = label_map, size = 4, data = arrowdf, show.legend = FALSE, check_overlap = F,position = position_jitter(width = .2, height = .2)) +
  geom_text_repel(mapping = label_map, size = 4, data = arrowdf, show.legend = FALSE) +
  theme_bw() +
  labs(title = paste0("Constrained Analysis of Principal Coordinates (",primer,")"), subtitle ="Species Differences Between Protocols", col = "Protocols") +
  ylab("CAP2 (15%)") +
  xlab("CAP1 (85%)")


ggsave(file = paste0("topanga/output/",primer,"/cap_mainDS_DNAindex",primer,".pdf"), plot = cap_plot, width = 12, height = 6)


#Permanova

adonis <- adonis(veganOTU_index ~ sampleDF$Site, data = sampleDF, permutations = 999, method = "bray")
write.table(adonis$aov.tab, file = paste0("topanga/output/",primer,"/permanova_DNAindex",primer,".txt"))

#Pairwise Permanova

pairAdonis <- pairwise_adonis(veganOTU_index, factors = sampleDF$Site, sim_method = "bray", p_adjust_m = "fdr")
write.table(pairAdonis, file = paste0("topanga/output/",primer,"/pairPermanova_DNAindex",primer,".txt"))


