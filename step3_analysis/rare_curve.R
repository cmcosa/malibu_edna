#Date: 06-Jul-2020
#Rarefaction of 12S for both runs

if (!require("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("DESeq2")

install.packages("remotes")
remotes::install_github("metabaRfactory/metabaR")

library(phyloseq)
library(ggplot2)
library(tidyverse)
library(ranacapa)
library(vegan)
library(pairwiseCI)
library(scales)


primer <- "16S"

physeq_mainDS <- readRDS(paste0("step3_analysis/output/",primer,"/physeq_mainDS_",primer,".Rds"))

#physeq_mergedSamples <- merge_samples(physeq_pruned, "Site")
#sample_data(physeq_mergedSamples)$Site <- sample_names(physeq_mergedSamples)

#saveRDS(physeq_mergedSamples, file = paste0("topanga/output/",primer,"/physeq_mergedSamples_",primer,".Rds"))


##################
#Rarefaction curve

rare_curve <- ggrare(physeq_mainDS, step = 5, se = TRUE, color = "Site") +
  geom_line(size = 1) +
  labs(title = primer, color = "Protocol", fill = "Protocol") +
  theme_bw()

ggsave(filename = paste0("step3_analysis/output/",primer,"/rare_curve_",primer,".pdf"), plot = rare_curve, width = 7, height = 5, units = "in")

#facet wrap

rare_curve_facet <- ggrare(physeq_mainDS, step = 5, se = TRUE, color = "Site") +
  geom_line(size = 1) +
  labs(title = primer, color = "Protocol", fill = "Protocol") +
  theme_bw() +
  facet_wrap(~Site)

ggsave(filename = paste0("step3_analysis/output/",primer,"/rare_curve_facet_",primer,".pdf"), plot = rare_curve_facet, width = 10, height = 5, units = "in")

#calculating slope

raremax <- min(rowSums(otu_table(physeq_mainDS)))
#rarecurve <- rarecurve(t(otu_table(physeq_pruned)), step = 5, sample = raremax)
x <- t(otu_table(physeq_mainDS))
rareslope_df <- as.data.frame(rareslope(x, sample = raremax))
protocol <- as.data.frame(c("NF3", "NF3", "NF3", "NF3", "NF3", "PF3", "PF3", "PF3", "PF3", "PF3", "SD", "SD", "SD", "SD", "SD"))
rareslope_df <- cbind(rareslope_df, protocol)
colnames(rareslope_df) <- c("slope", "protocol")

write.csv(rareslope_df, file = paste0("step3_analysis/output/",primer,"/rareslope_",primer,".csv"))

CI <- pairwiseCI(slope ~ protocol, data = rareslope_df, method = "Param.diff")
CI_df <- as.data.frame(CI)
write.csv(CI_df, file = paste0("step3_analysis/output/",primer,"/CI_pairwise_",primer,".csv"))

pdf(file = paste0("step3_analysis/output/",primer,"/CI_pairwise_plot_",primer,".pdf"), width = 8, height = 7)
plot(CI)
dev.off()


###############
#merged samples

rare_curve_merged <- ggrare(physeq_mergedSamples, step = 5, se = TRUE, color = "Site") +
  geom_line(size = 1) +
  labs(title = primer, color = "Protocol", fill = "Protocol") +
  theme_bw()

ggsave(file = paste0("step3_analysis/output/",primer,"/rare_curve_merged_",primer,".png"), plot = rare_curve_merged, width = 7, height = 5, units = "in")
